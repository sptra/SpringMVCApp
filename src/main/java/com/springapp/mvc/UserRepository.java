package com.springapp.mvc;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by sebastianvincent on 1/6/15.
 */
public interface UserRepository extends JpaRepository<User, Long>{

}
